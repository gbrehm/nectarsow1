<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/rdData">
        <rdData>
            <xsl:for-each select="ldSharedReportList">
                <widget id="{id}" type="{payload/type}" title="{payload/headerConfig/title}" created="{created}" updated="{updated}" owner="{createdBy/payload/userName}">
                    <xsl:attribute name="tags_markup" namespace="uri">
                        &lt;ul class="report-tag-list"&gt;
                            <xsl:for-each select="userTags/payload">
                                &lt;li&gt;
                                    &lt;div class="tag-name" title="<xsl:value-of select="name"/>"&gt;
                                        <xsl:value-of select="name"/>
                                    &lt;/div&gt;
                                &lt;/li&gt;
                            </xsl:for-each>
                        &lt;/ul&gt;
                    </xsl:attribute>
                    <xsl:attribute name="tags_list" namespace="uri">
                        <xsl:for-each select="userTags/payload"><xsl:value-of select="id"/>,</xsl:for-each>0
                    </xsl:attribute>
                </widget>
            </xsl:for-each>
        </rdData>
    </xsl:template>
</xsl:stylesheet>