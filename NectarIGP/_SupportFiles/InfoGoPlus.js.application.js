var logi = (function (window, $) {

	var toggleSidebar = function() {
		$('#igpSidebar').toggleClass('open');
		if ($('#igpSidebar').hasClass('open')) {
			localStorage.setItem('sidebarState', 'open');
		} else {
			localStorage.setItem('sidebarState', '');
		}
	};
	
	var refreshElement = function(id) {
		rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + id, 'false', '', true, null, null, ['','rdThemeWaitPanel','rdThemeWaitCaption'], true);
	};

	var deleteSelectedWidgets = function(list) {
		var deffered = new $.Deferred();
		deffered.resolve();
		
		list.each(function(i,widgetId) {
			deffered = deffered.then(function() {
				return deleteWidget(widgetId);
			});
		});

		return deffered.then(function(res) {
			return true;
		});
	}

    var apiRequest = function(objectPath, method, data) {
		return $.ajax({
					url:'rdTemplate/rdDiscovery/ngpDiscovery/api/platform/' + objectPath,
					type: method,
					contentType: 'application/json',
					data: (data) ? JSON.stringify(data) : {},
					dataType: 'json'
				});
	};

	// DELETE WIDGETS //
	var deleteWidget = function(id) {
		return apiRequest('system.configs/' + id, 'DELETE', {})
				.then(function(res) {
					return true;
				})
				.fail(function(err) {
					alert('There was an error while trying to delete the widget. Please try again');
					return false;
				});
	}

	// TAGGING //
	var addNewTag = function(widgetId, newTagName, relateTag) {
		return getExistingTagByName(newTagName)
				.then(function(existingTag) {
					if (!existingTag.length) {
						return apiRequest('system.tags', 'POST', {name: newTagName})
								.then(function(newTag) {
									if (relateTag) {
										// add the new tag to the widget
										return relateTagToWidget(widgetId, newTag.id);
									} else {
										return true;
									}
								})
								.fail(function(err) {
									alert('There was an error while trying to add your tag. Please try again');
									return false;
								});
					} else {
						return relateTagToWidget(widgetId, existingTag[0].id);
					}
				})
				.then(function(shouldRefresh) {
					if (shouldRefresh) {
						refreshElement('igpReportContent');
					}
				});
	};
	
	var relateTagToWidget = function(widgetId, tagId) {
		return apiRequest('system.relationships', 'POST', {
					"parentId": widgetId,
					"childId": tagId,
					"linkType": 'userTags'
				})
				.then(function(res) {
					refreshElement('igpReportContent');
					return false;
				})
				.fail(function(err) {
					alert('There was an error while trying to add your tag. Please try again');
					return err;
				});
	};
	
	var deleteTag = function(tagId) {
		apiRequest('system.tags/' + tagId, 'DELETE')
			.then(function(res) {   
				return refreshElement('igpReportContent');
			})
			.fail(function(err) {
				alert('There was an error while trying to delete your tag. Please try again');
				return false;
			});
	};
	
	var removeTag = function(widgetId, tagId) {
		apiRequest("system.relationships?$filter=linkType eq 'userTags' and parentId eq '" + widgetId + "' and childId eq '" + tagId + "'", 'GET')
			.then(function(res) {
				if (res.length && res[0].id) {
					return apiRequest('system.relationships/' + res[0].id, 'DELETE')
						.then(function(res) {
							return refreshElement('divReportList');
						})
						.fail(function(err) {
							alert('There was an error while trying to remove your tag. Please try again');
							return false;
						});
				} else {
					return false;
				}
			})
			.fail(function(err) {
				alert('There was an error while trying to remove your tag. Please try again');
				return false;
			});
	};
	
	// SHARING //
	var modifyWidgetPermissions = function(widgetId, entity, entityType, newPrivilegeLevel) {
		var permissionActions = {
			edit: ['read', 'render', 'update'],
			read: ['read', 'render'],
			none: []
		};
	
		// get all permissions on widget
		return apiRequest('system.configs/' + widgetId + '/security', 'GET')
				.then(function(res) {
					// filter permissions to the requested user entity
					var permissionsData = res.filter(function(perm) { return perm.type === 'permission' && perm.entity === entity && ['read','render', 'update'].indexOf(perm.action) !== -1 });
					var currentPrivilegeLevel = determineCurrentPrivilegeLevel(permissionsData);
					
					// check if the permissions need to change
					if (newPrivilegeLevel && currentPrivilegeLevel !== newPrivilegeLevel) {
						// check the difference between current 
						var permissions = null,
							action=null,
							permissionsToAdd = permissionActions[newPrivilegeLevel].filter(function(perm) { return permissionActions[currentPrivilegeLevel].indexOf( perm ) == -1 }),
							permissionsToRemove = permissionActions[currentPrivilegeLevel].filter(function(perm) { return permissionActions[newPrivilegeLevel].indexOf( perm ) == -1 });

						if (permissionsToAdd.length) {
							return addWidgetPermissions(widgetId, entity, entityType, permissionsToAdd);
						} else {
							var permissionIds = permissionsData
													.filter(function(perm) { return permissionsToRemove.indexOf( perm.action ) !== -1 })
													.map(function(perm) { return perm.id });
							return removeWidgetPermissions(widgetId, entity, permissionIds);
						}
					} else {
						// no changes to the permissions
						return false;
					}
				})
				.fail(function(err) {
					return false
				});
	};
	
	var addWidgetPermissions = function(widgetId, entity, entityType, privileges) {
		var deffered = new $.Deferred();
		deffered.resolve();

		privileges.forEach(function(permission) {
			deffered = deffered.then(function() {
				var postObj = {
					object: widgetId,
					entityType: entityType,
					entity: entity,
					type: 'permission',
					action: permission
				};
				// this return is key, it will make it so that this "then" call resolves or rejects as the apirequest does
				return apiRequest('system.configs/' + widgetId + '/security', 'POST', postObj)
						.then(function(res) {
							return true;
						})
						.fail(function(err) {
							return false;
						});
			});
		});

		return deffered.then(function(res) {
					return true;
				});
	};
	
	var removeWidgetPermissions = function(widgetId, entity, permissions) {
		var deffered = new $.Deferred();
		deffered.resolve();
		
		permissions.forEach(function(permissionId) {
			deffered = deffered.then(function() {
				return apiRequest('system.configs/' + widgetId + '/security/' + permissionId, 'DELETE')
						.then(function(res) {
							return true;
						})
						.fail(function(err) {
							return false;
						});
			});
		});

		return deffered.then(function(res) {
			return true;
		});
	};

	var isShared = function(widgetId) {
		// TODO: logic needs to be refined
		return false;
		/*
		return apiRequest('system.configs/' + widgetId + '/security', 'GET')
				.then(function(permissions) {
					var uniqueUsers = permissions
										.map(function(permission) { return permission.entity; })
										.filter(function(entity, index, array) { return array.indexOf(entity) == index && entity !== 'syspublic'; })
										.length;
					return uniqueUsers > 2 ? true : false;
				})
				.fail(function(err){
					return false;
				});
		*/
	}

	// private functions not included outside of this library
	function determineCurrentPrivilegeLevel(permissionsArray) {
		var permissions = permissionsArray.map(function(perm) { return perm.action });

		if (permissions.length) {
			if (permissions.length === 3 && (permissions.indexOf('read') !== -1 && permissions.indexOf('render') !== -1 && permissions.indexOf('update') !== -1)) {
				return 'edit';
			} else if (permissions.length === 2 && (permissions.indexOf('read') !== -1 && permissions.indexOf('render') !== -1)) {
				return 'read';
			}
		} else {
			return 'none';
		}
	};

	function getExistingTagByName(tagName) {
		return apiRequest('system.tags', 'GET')
				.then(function(tags) {
					return tags.filter(function(elm) { return elm.payload.name.toString().toLowerCase() === tagName.toLowerCase(); }).map(function(tag) { return tag; });
				})
				.fail(function(err){
					return false;
				});
	}

    return {
		toggleSidebar: toggleSidebar,
		refreshElement: refreshElement,
		deleteSelectedWidgets: deleteSelectedWidgets,
		apiRequest: apiRequest,
		deleteWidget: deleteWidget,
		addNewTag: addNewTag,
		relateTagToWidget: relateTagToWidget,
		deleteTag: deleteTag,
		removeTag: removeTag,
		modifyWidgetPermissions: modifyWidgetPermissions,
		isShared: isShared
	};
})(window, jQuery);